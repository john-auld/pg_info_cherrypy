import cherrypy
import psycopg2
import json
import sys
from config import db

cherrypy.config.update({
    'server.socket_host': '0.0.0.0',
    'server.socket_port': 8080,
})

def connect(thread_index): 
    # Create a connection and store it in the current thread 
    try:
        cherrypy.thread_data.db = psycopg2.connect(dbname=db['name'], user=db['user'], host=db['host'], password=db['password']) 
    except:
        print "Unable to connect to the database"
        sys.exit(1)
 
# Tell CherryPy to call "connect" for each thread, when it starts up 
cherrypy.engine.subscribe('start_thread', connect)


class PostgresqlInfo(object):
    @cherrypy.expose
    def index(self):
        return db['host']

    # query string test
    @cherrypy.expose
    def qs(self, qsvar=''):
        return qsvar

    @cherrypy.expose
    def databases(self, server=None):
        cherrypy.response.headers["Content-Type"] = "application/json"
        cur = cherrypy.thread_data.db.cursor()
        try:
            cur.execute("""SELECT datname FROM pg_database 
				WHERE datname != 'postgres' AND
				datname != 'pgdb' AND
				datname not like 'template%';""")
        except:
            print "SQL query failed"
            sys.exit(1)

        result = cur.fetchall()

        db_list = []
	for datname in result:
            db_list.append(datname[0])

	db_list.sort()
        return json.dumps(db_list)

    @cherrypy.expose
    def roles(self):
        cherrypy.response.headers["Content-Type"] = "application/json"
        cur = cherrypy.thread_data.db.cursor()
        try:
            cur.execute("""SELECT rolname FROM pg_roles
				WHERE rolname != 'postgres' AND
				rolname != 'pguser';""")
        except:
            print "SQL query failed"
            sys.exit(1)

        result = cur.fetchall()

        role = []
        for datname in result:
            role.append(datname[0])

        role.sort()
        return json.dumps(role)

if __name__ == '__main__':
    cherrypy.quickstart(PostgresqlInfo())
